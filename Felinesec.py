#   Felinesec.py
#   Python 3.7
#   Version 0.0.6
#
#   Created by Francesco Masala and (Others) on 19/02/19 for www.github.com/felinesec
#   GNU General Public License v3.0
#
import os, time, requests, argparse
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError
from colorama import Fore, Style

class Intro:
    os.system('cls' if os.name == 'nt' else 'clear')
    print(Fore.CYAN + " /$$$$$$$$        /$$ /$$                      /$$$$$$                          ")
    print("| $$_____/       | $$|__/                     /$$__  $$                         ")
    print("| $$     /$$$$$$ | $$ /$$ /$$$$$$$   /$$$$$$ | $$  \__/  /$$$$$$   /$$$$$$$     ")
    print("| $$$$$ /$$__  $$| $$| $$| $$__  $$ /$$__  $$|  $$$$$$  /$$__  $$ /$$_____/     ")
    print("| $$__/| $$$$$$$$| $$| $$| $$  \ $$| $$$$$$$$ \____  $$| $$$$$$$$| $$           ")
    print("| $$   | $$_____/| $$| $$| $$  | $$| $$_____/ /$$  \ $$| $$_____/| $$           ")
    print("| $$   |  $$$$$$$| $$| $$| $$  | $$|  $$$$$$$|  $$$$$$/|  $$$$$$$|  $$$$$$$     ")
    print("|__/    \_______/|__/|__/|__/  |__/ \_______/ \______/  \_______/ \_______/     ")
    print("                                                                                ")
    print(Style.RESET_ALL)
    print("Coded with <3 by www.github.com/francescomasala for www.github.com/felinesec")
    Language = input("Language selection:\n[I]talian\n[E]nglish\nOr [X] to eXit\n").upper()
    if Language == 'I':
        os.system('cls' if os.name == 'nt' else 'clear')
        print(Fore.CYAN + " /$$$$$$$$        /$$ /$$                      /$$$$$$                          ")
        print("| $$_____/       | $$|__/                     /$$__  $$                         ")
        print("| $$     /$$$$$$ | $$ /$$ /$$$$$$$   /$$$$$$ | $$  \__/  /$$$$$$   /$$$$$$$     ")
        print("| $$$$$ /$$__  $$| $$| $$| $$__  $$ /$$__  $$|  $$$$$$  /$$__  $$ /$$_____/     ")
        print("| $$__/| $$$$$$$$| $$| $$| $$  \ $$| $$$$$$$$ \____  $$| $$$$$$$$| $$           ")
        print("| $$   | $$_____/| $$| $$| $$  | $$| $$_____/ /$$  \ $$| $$_____/| $$           ")
        print("| $$   |  $$$$$$$| $$| $$| $$  | $$|  $$$$$$$|  $$$$$$/|  $$$$$$$|  $$$$$$$     ")
        print("|__/    \_______/|__/|__/|__/  |__/ \_______/ \______/  \_______/ \_______/     ")
        print("                                                                                ")
        print(Style.RESET_ALL)
        print("Benvenuto!\r")
        time.sleep(1)
        Website = input("Inserisci il sito da attaccare in questo formato (https//example.com):\n")
        print("Controllo se il sito indicato risulta online...\r")
        time.sleep(0.5)
        req = Request(Website)
        try:
            response = urlopen(req)
        except HTTPError as e:
            print('Il server ha soddisfatto la richiesta con il seguente codice d\'errore:\r'+ e.code)
        except URLError as e:
            print('Non ho trovato il server per la determinata ragione:\r' + e.reason)
        else:
            print('Il sito funziona correttamente\n')
        time.sleep(0.5)
        Attacco = input("Scegli la tipologia d'attacco\n1) CMS Control\n2) Wordpress Plugin Scan\n3)Sql Injection\n9) to exit\n").upper()
        if Attacco == '1':
            print("Menù controllo CMS\r")
            print("Attualmente non disponibile")
            time.sleep(1)
            exit()
        elif Attacco == '2':
            print("Menù Scan plugin wordpress\r")
            print("Attualmente non disponibile")
            time.sleep(1)
            exit()
        elif Attacco == '3':
            print("Menù SQL injection\r")
            print("Attualmente non disponibile")
            time.sleep(1)
            exit()
        elif Attacco == '9':
            print("Sto uscendo dal programma...")
            exit()
        elif Attacco != "":
            print("Scelta non valida\r")
            time.sleep(1)
            print("Sto uscendo dal programma...")
            time.sleep(1)
            exit()
    elif Language == 'E':
        os.system('cls' if os.name == 'nt' else 'clear')
        print(Fore.CYAN + " /$$$$$$$$        /$$ /$$                      /$$$$$$                          ")
        print("| $$_____/       | $$|__/                     /$$__  $$                         ")
        print("| $$     /$$$$$$ | $$ /$$ /$$$$$$$   /$$$$$$ | $$  \__/  /$$$$$$   /$$$$$$$     ")
        print("| $$$$$ /$$__  $$| $$| $$| $$__  $$ /$$__  $$|  $$$$$$  /$$__  $$ /$$_____/     ")
        print("| $$__/| $$$$$$$$| $$| $$| $$  \ $$| $$$$$$$$ \____  $$| $$$$$$$$| $$           ")
        print("| $$   | $$_____/| $$| $$| $$  | $$| $$_____/ /$$  \ $$| $$_____/| $$           ")
        print("| $$   |  $$$$$$$| $$| $$| $$  | $$|  $$$$$$$|  $$$$$$/|  $$$$$$$|  $$$$$$$     ")
        print("|__/    \_______/|__/|__/|__/  |__/ \_______/ \______/  \_______/ \_______/     ")
        print("                                                                                ")
        print(Style.RESET_ALL)
        print("Welcome!\n")
        time.sleep(0.5)
        Website = input("Please insert the site or IP you would like to attack with this format (https://example.com):\n")
        time.sleep(0.5)
        req = Request(Website)
        try:
            response = urlopen(req)
        except HTTPError as e:
            print('The server satisfied the request with the following error:\r'+ e.code)
        except URLError as e:
            print('I did not find the server with the given reason:\r' + e.reason)
        else:
            print('The website/server is working correctly\n')
        Attacco = input("Choose the type of attack\n1) CMS Control\n2) Wordpress Plugin Scan\n3)Sql Injection\n9) to exit\n").upper()
        if Attacco == '1':
            print("CMS checkup\r")
            print("Under maintenance")
            time.sleep(1)
            exit()
        elif Attacco == '2':
            print("Menù Scan plugin wordpress\r")
            print("Under maintenance")
            time.sleep(1)
            exit()
        elif Attacco == '3':
            print("Menù SQL injection\r")
            print("Under maintenance")
            time.sleep(1)
            exit()
        elif Attacco == '9':
            print("Exiting now")
            exit()
        elif Attacco != "":
            print("Choice not valid\r")
            time.sleep(1)
            print("Exiting now")
            time.sleep(1)
            exit()
    elif Language == 'X':
        print("Exiting now")
        exit()
    elif Language != "":
        print("Choice not valid\r")
        time.sleep(1)
        print("Exiting now")
        time.sleep(1)
        exit()
